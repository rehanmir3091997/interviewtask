package com.example.interviewtask;

import com.example.interviewtask.Model.object;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JSONPlaceHolder {
    @GET("repositories?q=language=+sort:stars")
    Call<object> getObject();
}
